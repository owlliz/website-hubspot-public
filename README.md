# Owl Labs HubSpot pages

## Instructions

1. Run `npm i` to install.
2. Either use the provided dev or staging HubSpot portal, or [create your own developer app](https://developers.hubspot.com/docs/faq/how-do-i-create-an-app-in-hubspot).
    - If you create your own portal, create a test account within your new portal. This will enable you to use the Design Manager, create pages, etc.
3. Rename `hubspot.config.SAMPLE.yml` to `hubspot.config.yml` file.
4. Authenticate to your `DEV` portal by running `npm run auth`, entering your portal ID and client data when prompted.
5. Run `npm start` to compile and upload assets to your dev portal. Changes will be live immediately after the script runs. All of your module changes should be made in `src/modules/[MODULE_NAME]/assets` — do **not** edit the `build` folder directly as it will be overwritten.

## Creating page-specific styles/JS

One day we'll scriptify this, but for now the process is manual (but quick!).

1. Create a directory within `src/template-assets` that is the same name as the template file (e.g. `templates/homepage.html` would have a directory of `src/template-assets/homepage`).
2. Add an `assets/` directory inside of the folder you just created.
3. Add an `assets/index.js` file (`src/template-assets/[templateName]/assets/index.js`) containing the following: `import './styles/index';`. Add JS here or as imports.
4. Create a `assets/styles/index.scss` sass file. Add Scss here.

## Creating new modules

It's usually easiest to create a module in the Design Manager, then `fetch` it to this repo. To do so:

1. Create the module within `Owl Web Team/modules`
2. In this directory, run `hscms fetch --portal=DEV src/modules/`.
3. To use Scss and JS compilation, autoprefixing, etc. for this module, add an `assets/` directory inside of the fetched folder.
4. Add an `assets/index.js` file (`src/modules/[moduleName].module/assets/index.js`) containing the following: `import './styles/index';`. Add JS here or as imports.
5. Create a `assets/styles/index.scss` sass file. Add Scss here.
6. Remove `module.css` and `module.js` in this directory; these will get taken care of by the build.

Once you finalize building a new module, it can be easily transferred to other portals or repositories. To include it on a page, be sure the `Make available in templates and pages` toggle is active within the Design Manager (this is the same option as `is_available_for_new_content: true` in the module's `meta.json` file).

## Importing common Owl Labs code

### Common npm package

Common Scss/JS code is stored in the `@owl-labs/web-common-public` npm. See below for some highlights and tips on using it.

### Scss

We use Scss for all styles. View the documentation at `@node_modules/@owl-labs/web-common-public/sassdoc/index.html`.

Import the variables file in any Scss document to have access to the breakpoint mixins, color variables, and other variables, for example:
```
@import '~@owl-labs/web-common-public/scss/variables';
```

#### Breakpoints

Try to code mobile-first, using `$breakpointAbove($breakpointTablet)` (or a different parameter for the viewport width).

#### Colors

The `variables.scss` file will import several variables. Try to use colors from `_theme.scss` instead of `_palette.scss` to make reskinning/retheming easier in future.

### JS

We use ES6 and transpile via Babel. **We do not use jQuery!** Feel free to use lightweight npm packages if needed. 

Although we haven't set up JSDoc [yet], importing common JS is fairly straightforward. For example:
```
import toggleClass from '@owl-labs/web-common-public/js/utils/toggle-class';
```

Have a module with styles but no JS? Add `/* disable-js-build */` as the first line of a `assets/index.js` file to prevent JS from building.

## Resources
* [Local development](https://designers.hubspot.com/docs/tools/local-development) 
* [HubSpot CMS Developer Sandbox](https://offers.hubspot.com/free-cms-developer-sandbox)

## TODOs

This is a very early draft. We've got a lot to do still, including:

- [ ] Deploy only changed files as part of `watch` process
- [ ] JS linting in shared npm
- [ ] Scss linting in shared npm

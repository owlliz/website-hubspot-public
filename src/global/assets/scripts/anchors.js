import smoothScroll from "./smooth-scroll";

const anchorLinks = document.querySelectorAll('a[href*="#"]:not([href="#"])');
if (anchorLinks.length) {
  [].forEach.call(anchorLinks, link => {
    link.addEventListener("click", e => {
      const clickedLink = e.currentTarget;
      const isAnchorOnPage =
        document.location.pathname.replace(/^\//, "") ===
          clickedLink.pathname.replace(/^\//, "") &&
        document.location.hostname === clickedLink.hostname;

      if (isAnchorOnPage) {
        e.preventDefault();
        triggerSmoothScroll(clickedLink);
      }
    });
  });
}

const triggerSmoothScroll = link => {
  const target = document.querySelector(
    `${link.hash}, [id="${link.hash.slice(1)}"], [name="${link.hash.slice(1)}"]`
  );

  if (target) {
    const windowWidth = Math.min(
      window.innerWidth,
      document.documentElement.clientWidth
    );

    // Determine fixed element offsets
    const fixedElements = document.querySelectorAll(
      '[data-fixed-element="placeholder"]'
    );
    const fixedElementsOffset =
      fixedElements.length && windowWidth < 1025
        ? [].reduce.call(
            fixedElements,
            (prev, el) => {
              // Only include navs that will be passed/stuck after scroll distance
              const elOffsetTop = Math.max(
                el.offsetTop,
                el.getBoundingClientRect().top + window.pageYOffset
              );
              const targetOffsetTop = Math.max(
                target.offsetTop,
                target.getBoundingClientRect().top + window.pageYOffset
              );

              if (elOffsetTop <= targetOffsetTop) prev += el.offsetHeight; // eslint-disable-line no-param-reassign, max-len
              return prev;
            },
            0
          )
        : 0;

    // Scroll to target
    smoothScroll(target, {
      offset: fixedElementsOffset
    });
    return false;
  }
  return true;
};

/*
 * Usage:
   <div class="form-embed" data-css-class="some-class" data-portal-id="2799205" data-form-id="dde7f579-de7b-4e02-b281-6e212bcfb44d" id="subscribe-footer"></div>

  - Required: ID
*/

import loadResource from "load-resource";

const forms = [...document.getElementsByClassName("form-embed")];

if (forms.length) {
  loadResource.load("https://js.hsforms.net/forms/v2.js").then(() => {
    forms.forEach(form => {
      const { id } = form;
      const {
        cssClass = "",
        portalId = "2799205",
        formId = "dde7f579-de7b-4e02-b281-6e212bcfb44d",
        formInstanceId = id
      } = form.dataset;

      hbspt.forms.create({
        portalId,
        formId,
        formInstanceId,
        cssClass,
        target: `#${id}`,
        css: ""
      });
    });
  });
}

/* global Element requestAnimationFrame */

/**
 * Usage Guide
 *
 * @param {HTMLElement} destination
 * @description - the element to scroll to
 *
 * @param {int} offset
 * @description - the distance to offset the scroll destination.
 * @default - 0
 *
 * @param {int} duration
 * @description - the speed of the scrolling animation.
 * @default - 1000
 *
 * @param {string} easing
 * @description - the easing function to apply to the scrolling animation.
 * @default - 'ease'
 *
 * @param {function} cb
 * @description - an optional callback function once scroll animation is completed.
 *
 * */

export default (destination, opts = {}, cb) => {
  const { offset = 0, duration = 1000, easing = "easeCubic" } = opts;

  const easingFunctions = {
    linear(t) {
      return t;
    },
    easeQuad(t) {
      // quad
      return t < 0.5 ? 2 * t * t : (-1 + (4 - 2 * t)) * t;
    },
    easeCubic(t) {
      // cubic
      return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
    }
  };

  if (destination === undefined)
    throw new Error("Scroll destination is required.");

  const destinationIsNumber = typeof destination === "number";
  const destinationIsElement =
    typeof destination === "object" && destination instanceof Element;

  const destinationIsValid = destinationIsElement || destinationIsNumber;

  // Destination can be a node element or number value (will scroll that distance)
  if (destination && destinationIsValid) {
    const startPosition = window.pageYOffset;
    const startTime = Date.now();

    const docHeight = Math.max(
      document.body.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.clientHeight,
      document.documentElement.scrollHeight,
      document.documentElement.offsetHeight
    );

    const windowHeight = window.innerHeight;
    const destinationValueAsNumber = destinationIsNumber
      ? destination
      : Math.max(
          destination.offsetTop,
          destination.getBoundingClientRect().top + window.pageYOffset
        );

    const destinationOffset = destinationValueAsNumber - offset;

    let offsetToScroll;

    if (docHeight - destinationOffset < windowHeight) {
      offsetToScroll = Math.round(docHeight - windowHeight);
    } else {
      offsetToScroll = Math.round(destinationOffset);
    }

    // If 'requestAnimationFrame' not supported
    if (!("requestAnimationFrame" in window)) {
      window.scroll(0, offsetToScroll);
      if (cb) cb();
      return;
    }

    // Scroll function
    const scroll = () => {
      const currentTime = Date.now();
      const elapsedTime = Math.min(1, (currentTime - startTime) / duration);
      const timingFunction = easingFunctions[easing](elapsedTime);
      const scrollDestination = Math.ceil(
        timingFunction * (offsetToScroll - startPosition) + startPosition
      );

      // Scroll window to iterative destination
      window.scroll(0, scrollDestination);

      // Trigger optional callback at end
      if (
        Math.ceil(window.pageYOffset) === offsetToScroll ||
        currentTime - startTime > duration
      ) {
        if (cb) cb();
        return;
      }
      requestAnimationFrame(scroll);
    };

    scroll();
  } else {
    throw new Error("Scroll destination is not a valid object or number.");
  }
};

export default () => {
  const isInViewport = el => {
    let { offsetTop, offsetLeft } = el;
    const { offsetHeight, offsetWidth } = el;

    while (el.offsetParent) {
      el = el.offsetParent; // eslint-disable-line no-param-reassign
      offsetTop += el.offsetTop;
      offsetLeft += el.offsetLeft;
    }

    return (
      offsetTop < window.pageYOffset + window.innerHeight &&
      offsetLeft < window.pageXOffset + window.innerWidth &&
      offsetTop + offsetHeight > window.pageYOffset &&
      offsetLeft + offsetWidth > window.pageXOffset
    );
  };

  const items = document.querySelectorAll('*[data-animate-in]');
  let waiting = false;
  const w = window;

  const detection = () => {
    for (let i = 0; i < items.length; i++) {
      const el = items[i];
      if (el.className.indexOf('viewed') === -1) {
        if (isInViewport(el)) {
          el.classList.add('viewed');
        } else {
          el.classList.remove('viewed');
        }
      }
    }
  };

  w.addEventListener('scroll', () => {
    if (waiting) {
      return;
    }
    waiting = true;
    detection();

    setTimeout(() => {
      waiting = false;
    }, 100);
  });

  setTimeout(() => {
    detection();
  }, 500);

  for (let i = 0; i < items.length; i++) {
    let d = 0;
    const el = items[i];
    if (items[i].getAttribute('data-animate-in-delay')) {
      d = items[i].getAttribute('data-animate-in-delay') / 1000;
    } else {
      d = 0;
    }
    el.setAttribute('style', `transition-delay: ${d}s`);
  }
};

import OnScreen from "onscreen";

const os = new OnScreen({
  tolerance: 0,
  debounce: 300,
  container: window
});

const selector = ".detect-in-view";
os.on("enter", selector, (element, event) => {
  element.classList.add("in-view");
});

os.on("leave", selector, (element, event) => {
  element.classList.remove("in-view");
});

import toggleClass from "@owl-labs/web-common/js/utils/toggle-class";

const visibleEls = [
  ...document.getElementsByClassName("currency--selected-value")
];
const optionEls = [
  ...document.getElementsByClassName("currency-dropdown__link")
];
const currencyEls = [...document.getElementsByClassName("currency")];
const isTouch = document.documentElement.classList.contains("touch");

optionEls.forEach(optionEl => {
  optionEl.addEventListener("click", evt => {
    // Get currency code from clicked link
    const currCode = optionEl.innerText;

    // Set each visible menu item to new currency code
    visibleEls.forEach(
      visibleEl => (visibleEl.dataset.selectedCurrency = currCode)
    );

    // Close dropdown on touch devices
    if (isTouch) {
      toggleClass(optionEl.parentElement, "hover");
    }

    // Reset active element
    const currentActiveEls = optionEls.filter(el =>
      el.classList.contains("active")
    );
    currentActiveEls.forEach(el => el.classList.remove("active"));

    // Add active state to all links with this currency code
    const inactiveEls = optionEls.filter(el => el.dataset.code === currCode);
    inactiveEls.forEach(el => {
      el.classList.add("active");
    });

    // Set non-nav currency nodes to selected code
    currencyEls.forEach(currencyEl => {
      currencyEl.dataset.selectedCurrency = currCode;
    });
  });
});

if (isTouch) {
  visibleEls.forEach(visibleEl => {
    visibleEl.addEventListener("click", () =>
      toggleClass(visibleEl.parentElement, "hover")
    );
  });
}

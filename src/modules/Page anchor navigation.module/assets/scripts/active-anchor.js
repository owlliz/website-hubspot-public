import OnScreen from "onscreen";
import memoizeOne from "memoize-one";
import toggleClass from "@owl-labs/web-common/js/utils/toggle-class";

// DOM selectors
const prefix = "page-anchor-nav";
const navEl = document.querySelector(`.${prefix}`);
const anchors = [...navEl.getElementsByClassName(`${prefix}__anchor`)];
const selector = `.${prefix}__section[id]`;
const currentNavEl = navEl.querySelector(`.${prefix}__current`);

// Determine anchor nav offset
const debounce = (func, wait, immediate) => {
  let timeout;
  return function() {
    const context = this;
    const args = arguments;
    const later = () => {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) {
      func.apply(context, args);
    }
  };
};
const setHeight = () => {
  const headerHeight = document.getElementsByClassName("header")[0]
    .clientHeight;
  navEl.style.top = `${headerHeight}px`;
};
const setHeightDebounce = debounce(setHeight, 300);

// Add resize event
window.addEventListener("resize", setHeightDebounce);
setHeight();

// Fetch matching anchor link from section ID
const getAnchorLink = elementId => {
  const anchorEl = anchors.find(anchor => elementId === anchor.hash.substr(1));
  if (typeof anchorEl !== "undefined") {
    return anchorEl;
  }
  throw new Error(`No matching anchor found for section with Id ${elementId}`);
};
// Memoized version of function
const _getAnchorLink = memoizeOne(getAnchorLink);

// Event listeners for sections
const os = new OnScreen({
  tolerance: 0,
  debounce: 250,
  container: window
});

os.on("enter", selector, ({ id }, event) => {
  const anchorEl = _getAnchorLink(id);
  if (!anchorEl.classList.contains("active")) {
    const activeAnchor = anchors.find(anchor =>
      anchor.classList.contains("active")
    );
    if (typeof activeAnchor !== "undefined") {
      activeAnchor.classList.remove("active");
    }
    anchorEl.classList.add("active");
    currentNavEl.innerText = anchorEl.innerText;
  }
});

// Mobile accordion
currentNavEl.addEventListener("click", evt => {
  evt.preventDefault();
  toggleClass(navEl, "expanded");
});

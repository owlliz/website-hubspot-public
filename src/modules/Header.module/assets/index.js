import "./styles/index.scss";

// TODO: remove when S2 header removed
if (document.body.className.includes("hs-")) {
  document.documentElement.classList.add("no-touch");
}

import "@owl-labs/web-common/dist/feature-detection";
import "@owl-labs/web-common/dist/mobile-menu";
import "@owl-labs/web-common/dist/utm";
import "./scripts/currency";

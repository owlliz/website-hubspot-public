const shell = require("shelljs");
const path = require("path");

const commandNotFound = () => {
  shell.echo("Please pass in a command: 'build' or 'watch'");
  shell.exit(1);
};

const buildAssets = (outDir, command = commandNotFound()) => {
  const dir = path.resolve(__dirname, `../src/${outDir}`);
  shell.cd(dir);
  const isGlobal = outDir === "global";
  const isModule = outDir === "modules";
  shell.ls("**/*/index.js").forEach(srcFile => {
    // Handle spaces in path
    const srcFileFmt = srcFile.replace(/(\s+)/g, "\\$1");

    // Get file and directory info
    const currentFile = path.resolve(dir, srcFileFmt);
    const buildDir = path.dirname(
      path.resolve(__dirname, `../build/${outDir}`, srcFileFmt, "../")
    );

    // Build or watch
    const bundle = shell.exec(
      `parcel ${command} ${currentFile} --out-dir ${buildDir} -o ${
        isGlobal ? "global.js" : isModule ? "module.js" : "bundle.js"
      } --no-source-maps --no-cache`
    );

    // Remove assets folder from build directory
    shell.rm("-rf", `${buildDir}/assets`);

    const { code: isEnabled } = shell.exec(
      `head -1 ${currentFile} | grep disable-js-build`,
      { silent: true }
    );
    if (!isEnabled) {
      // Empty file contents (modules only)
      shell.exec(`> ${buildDir}/module.js`);
    }
  });
};

module.exports = command => {
  // Copy files to release dir
  shell.rm("-rf", "build");
  shell.cp("-R", "src/", "build/");

  // Build module, global, and template assets
  buildAssets("template-assets", command);
  buildAssets("modules", command);
  buildAssets("global", command);
};
